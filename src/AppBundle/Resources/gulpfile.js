'use strict';

require('es6-promise').polyfill();

var gulp = require('gulp');
var gulpsync = require('gulp-sync')(gulp);
var streamqueue = require('streamqueue');
var clean = require('gulp-clean');
var concat = require('gulp-concat');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var concatCss = require('gulp-concat-css');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var replace = require('gulp-replace');

var dest = '../../../web';

var autoprefixerOptions = {
    browsers: ['last 2 versions', '> 5%', 'Firefox ESR']
};

gulp.task('default', gulpsync.sync([
    ['clean'],
    ['jshint'],
    ['css', 'js', 'fonts', 'images']
]));

gulp.task('clean', function () {
    return gulp.src([
        dest + '/css',
        dest + '/fonts',
        dest + '/js',
        dest + '/images'
    ])
        .pipe(clean({force: true}));
});

gulp.task('jshint', function () {
    return streamqueue({objectMode: true},
        gulp.src('js/*.js')
    )
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('css', function () {
    return streamqueue({objectMode: true},
        gulp.src('css/base.scss'),
        gulp.src('css/*.css')
    )
        .pipe(sass({
            outputStyle: 'compressed',
            includePaths: [
                'bower_components/bootstrap-sass/assets/stylesheets',
                'bower_components/font-awesome/scss'
            ]
        }).on('error', sass.logError))
        .pipe(concatCss('styles.css'))
        .pipe(autoprefixer(autoprefixerOptions))
        .pipe(replace('/*!', '/*'))
        .pipe(minifyCss())
        .pipe(gulp.dest(dest + '/css'));
});

gulp.task('fonts', function () {
    return gulp.src([
        'fonts/*.*',
        'bower_components/bootstrap-sass/assets/fonts/bootstrap/*.*',
        'bower_components/font-awesome/fonts/*.*'
    ])
        .pipe(gulp.dest(dest + '/fonts'));
});

gulp.task('images', function () {
    return gulp.src([
        'images/*.*'
    ])
        .pipe(gulp.dest(dest + '/images'));
});

gulp.task('js', function () {
    return streamqueue({objectMode: true},
        gulp.src('bower_components/jquery/dist/jquery.js'),
        gulp.src('bower_components/bootstrap-sass/assets/javascripts/bootstrap.js'),
        gulp.src('js/*.js')
    )
        .pipe(concat('scripts.js', {newLine: ";\n"}))
        .pipe(uglify())
        .pipe(gulp.dest(dest + '/js'));
});

gulp.task('watch', function () {
    gulp.watch('fonts/*.*', ['fonts']);
    gulp.watch('js/*.*', ['js']);
    gulp.watch('css/*.*', ['css']);
    gulp.watch('images/*.*', ['images']);
});
