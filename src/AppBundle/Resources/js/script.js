$(function () {
    $('button.form-action').click(function (event) {
        event.preventDefault();
        form = $('#' + $(this).data('form'));
        saveEmail($(this), form);
    });
});

function saveEmail(button, form) {
    sum = form.find('input[name="sum"]');
    sum.parents('.form-group').find('.form-group-errors').hide();
    if (!sum.val()) {
        sum.parents('.form-group').find('.form-group-errors').show();
    }

    email = form.find('input[name="email"]');
    email.parents('.form-group').find('.form-group-errors').hide();
    if (!email.val()) {
        email.parents('.form-group').find('.form-group-errors').show();
    }

    if (sum.val() && email.val()) {
        $.ajax({
            url: '/save-email',
            data: email.val(),
            type: 'POST',
            cache: false,
            processData: false,
            contentType: false
        })
            .done(function (response) {
                if (response && response.success) {
                    form.submit();
                } else {
                    alert('Что-то пошло не так, попробуйте позже.');
                }
            })
            .fail(function () {
                alert('Что-то пошло не так, попробуйте позже.');
            });
    }
}