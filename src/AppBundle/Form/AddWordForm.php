<?php
/**
 * Created by PhpStorm.
 * User: Пётр
 * Date: 19.04.2018
 * Time: 10:45
 */

namespace AppBundle\Form;

use AppBundle\Entity\Word;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddWordForm extends AbstractType
{
    /**
     * Создание формы
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('description', TextareaType::class)
            ->add('alternative', TextType::class, ['mapped' => false]);
    }

    /**
     * Сам пока не знаю, взял из примера
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => Word::class]);
    }
}
