<?php
/**
 * Created by PhpStorm.
 * User: Пётр
 * Date: 24.05.2018
 * Time: 22:51
 */

namespace AppBundle\Form;

use AppBundle\Entity\Alternative;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddAlternativeForm extends AbstractType
{
    /**
     * Создание формы
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class);
    }

    /**
     * Сам пока не знаю, взял из примера
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => Alternative::class]);
    }
}