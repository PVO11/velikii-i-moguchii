<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Benefactor;
use AppBundle\Entity\Word;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class BaseController extends Controller
{
    /**
     * @Route("/", name="index")
     * @Method("GET")
     */
    public function indexAction()
    {
        //$allWordForm = $this->createForm(AllWordForm::class, null);

        $em = $this->getDoctrine()->getManager();

        //$words = $em->getRepository('AppBundle:Word')->findAll();
        $words = $em->getRepository('AppBundle:Word')->findBy(
            array(),
            array('name' => 'ASC')
        );

        return $this->render('AppBundle::index.html.twig', array('words' => $words));
        //return $this->render('AppBundle::index.html.twig', ['allWordForm' => $allWordForm->createView()], array('words' => $words));
    }


    /**
     * @Route("/save-email", name="save-email")
     * @Method("POST")
     */
    public function saveEmailAction(Request $request)
    {
        try{
            $email = $request->getContent();

            if ($email)
            {
                $benefactor = new Benefactor();
                $benefactor->setEmail($email);

                $em = $this->getDoctrine()->getManager();
                $em->persist($benefactor);
                $em->flush();
            }
        }
        catch (\Exception $e)
        {
            // ...
        }

        return new JsonResponse(
            ['success' => true]
        );
    }
}