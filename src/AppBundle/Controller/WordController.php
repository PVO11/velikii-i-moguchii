<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Word;
use AppBundle\Entity\Alternative;
use AppBundle\Form\AddWordForm;
use AppBundle\Form\AddAlternativeForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class WordController extends Controller
{
    /**
     * @Route("/add-word", name="add-word")
     * @Method("GET")
     */
    public function addWordAction()
    {
        $addWordForm = $this->createForm(AddWordForm::class, null, ['action' => $this->generateUrl('add-word-process')]);

        return $this->render('AppBundle::add-word.html.twig', ['addWordForm' => $addWordForm->createView()]);
    }

    /**
     * @Route("/add-word", name="add-word-process")
     * @Method("POST")
     */
    public function addWordProcessAction(Request $request)
    {
        $word = new Word();
        $form = $this->createForm(AddWordForm::class, $word);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $alternative = new Alternative();
            $alternative->setName($form->get('alternative')->getData());
            $alternative->setRating(1);
            $alternative->setWord($word);

            $em = $this->getDoctrine()->getManager();

            $em->persist($word);
            $em->persist($alternative);
            $em->flush();

            $this->getDoctrine()->getRepository('AppBundle:Word')->findBy(
                array(),
                array('name' => 'ASC')
            );

            return $this->redirectToRoute('index');
        }

        return $this->render('AppBundle::add-word.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/self-word/{id}", name="self-word")
     * @Method("GET")
     */
    public function selfWordAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $wordRepository = $em->getRepository('AppBundle:Word');

        $word = $wordRepository->find($id);
        if (!$word) {
            throw new NotFoundHttpException('Слово id="'.$id.'" не найдено');
        }

        $addAlternativeForm = $this->createForm(AddAlternativeForm::class, null, ['action' => $this->generateUrl('add-alternative-process', ['id'=>$word->getId()])]);

        return $this->render(
            'AppBundle::self-word.html.twig',
            [
                'word' => $word,
                'addAlternativeForm' => $addAlternativeForm->createView(),
            ]
        );
    }

    /**
     * @Route("/delete-word/{id}", name="delete-word")
     * @Method("GET")
     */
    public function deleteWordAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $wordRepository = $em->getRepository('AppBundle:Word');

        $word = $wordRepository->find($id);
        if (!$word) {
            throw new NotFoundHttpException('Слово id="'.$id.'" не найдено');
        }

        $em->remove($word);
        $em->flush();

        return $this->redirectToRoute('index');
    }

    /**
     * @Route("/add-alternative/{id}", name="add-alternative-process")
     * @Method("POST")
     */
    public function addAlternativeProcessAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $wordRepository = $em->getRepository('AppBundle:Word');

        $word = $wordRepository->find($id);
        if (!$word) {
            throw new NotFoundHttpException('Слово id="'.$id.'" не найдено');
        }

        $alternative = new Alternative();
        $formA = $this->createForm(AddAlternativeForm::class, $alternative);
        $formA->handleRequest($request);

        if ($formA->isSubmitted() && $formA->isValid())
        {
            $alternative->setRating(1);
            $alternative->setWord($word);

            $em->persist($alternative);
            $em->flush();

            return $this->redirectToRoute('self-word', ['id'=>$alternative->getWord()->getId()]);
        }

        // var_dump $word; // !!! так и не попробовал, надо проверить!!! :)
        //echo $id;

        //echo $alternative->getName();
/*
        if ($formA->isSubmitted() && $formA->isValid())
        {
            $alternative->setName($formA->get('alternative')->getData());
            $alternative->setRating(1);
            $alternative->setWord($word);

            $em = $this->getDoctrine()->getManager();

            $em->persist($alternative);
            $em->flush();

            return $this->redirectToRoute('index');
        }*/

        return $this->redirectToRoute('self-word', ['id'=>$alternative->getWord()->getId()]);
        //return $this->redirectToRoute('index');
        //return $this->render('AppBundle::base.html.twig');
    }

    /**
     * @Route("/raise-alternative/{id}", name="raise-alternative")
     * @Method("GET")
     */
    public function raiseAlternativeAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $alternativeRepository = $em->getRepository('AppBundle:Alternative');

        $alternative = $alternativeRepository->find($id);
        if (!$alternative) {
            throw new NotFoundHttpException('Альтернатива id="'.$id.'" не найдена');
        }

        $alternative->setRating($alternative->getRating() + 1);

        $em->persist($alternative);
        $em->flush();

        return $this->redirectToRoute('self-word', ['id'=>$alternative->getWord()->getId()]);
    }

    /**
     * @Route("/lower-alternative/{id}", name="lower-alternative")
     * @Method("GET")
     */
    public function lowerAlternativeAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $alternativeRepository = $em->getRepository('AppBundle:Alternative');

        $alternative = $alternativeRepository->find($id);
        if (!$alternative) {
            throw new NotFoundHttpException('Альтернатива id="'.$id.'" не найдена');
        }

        $alternative->setRating($alternative->getRating() - 1);

        $em->persist($alternative);
        $em->flush();

        return $this->redirectToRoute('self-word', ['id'=>$alternative->getWord()->getId()]);
    }

    /**
     * @Route("/delete-alternative/{id}", name="delete-alternative")
     * @Method("GET")
     */
    public function deleteAlternativeAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $alternativeRepository = $em->getRepository('AppBundle:Alternative');

        $alternative = $alternativeRepository->find($id);
        if (!$alternative) {
            throw new NotFoundHttpException('Альтернатива id="'.$id.'" не найдена');
        }

        $em->remove($alternative);
        $em->flush();

        return $this->redirectToRoute('self-word', ['id'=>$alternative->getWord()->getId()]);
    }
}