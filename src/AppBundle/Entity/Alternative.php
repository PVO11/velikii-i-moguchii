<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AlternativeRepository")
 * @ORM\Table(
 *     options={"comment":"Альтернатива"}
 *     )
 */
class Alternative
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer", nullable=false, options={"unsigned"=true, "comment"="Идентификатор"})
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=128, nullable=false, options={"comment"="Имя"})
     */
    protected $name = '';


    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    protected $rating;

    /**
     * @ORM\ManyToOne(targetEntity="Word", inversedBy="alternative")
     * @ORM\JoinColumn(name="word_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $word;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=false, options={"comment"="Описание"})
     */
    protected $description = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false, options={"comment"="Дата и время создания заказа"})
     */
    protected $createdAt;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Alternative
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set rating.
     *
     * @param string $rating
     *
     * @return Alternative
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating.
     *
     * @return string
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set word.
     *
     * @param \AppBundle\Entity\Word|null $word
     *
     * @return Alternative
     */
    public function setWord(\AppBundle\Entity\Word $word = null)
    {
        $this->word = $word;

        return $this;
    }

    /**
     * Get word.
     *
     * @return \AppBundle\Entity\Word|null
     */
    public function getWord()
    {
        return $this->word;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }
}
