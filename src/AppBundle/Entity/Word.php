<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\WordRepository")
 * @ORM\Table(
 *     options={"comment":"Слова"}
 *     )
 */
class Word
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer", nullable=false, options={"unsigned"=true, "comment"="Идентификатор слова"})
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=128, nullable=false, options={"comment"="Имя"})
     */
    protected $name = '';

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=false, options={"comment"="Описание"})
     */
    protected $description = '';

    /**
     * @ORM\OneToMany(targetEntity="Alternative", mappedBy="word")
     * @ORM\OrderBy({"rating"="DESC"})
     */
    private $alternative;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false, options={"comment"="Дата и время создания заказа"})
     */
    protected $createdAt;

    public function __construct()
    {
        $this->alternative = new ArrayCollection();
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Word
     */
    public function setName(string $name): Word
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Word
     */
    public function setDescription(string $description): Word
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Add alternative.
     *
     * @param \AppBundle\Entity\Alternative $alternative
     *
     * @return Word
     */
    public function addAlternative(\AppBundle\Entity\Alternative $alternative)
    {
        $this->alternative[] = $alternative;

        return $this;
    }

    /**
     * Remove alternative.
     *
     * @param \AppBundle\Entity\Alternative $alternative
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeAlternative(\AppBundle\Entity\Alternative $alternative)
    {
        return $this->alternative->removeElement($alternative);
    }

    /**
     * Get alternative.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAlternative()
    {
        return $this->alternative;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }
}
